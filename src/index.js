import React from 'react';
import ReactDOM from 'react-dom';
import Joke from './components/Joke';

ReactDOM.render(<Joke />, document.getElementById('root'));
