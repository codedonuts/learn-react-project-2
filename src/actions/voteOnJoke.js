import actions from '../action-types/root';

const voteUp = () => ({type: actions.VOTE, direction: true});
const voteDown = () => ({type: actions.VOTE, direction: false});

export default {voteUp, voteDown};